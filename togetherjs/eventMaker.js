/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

define(["jquery", "util"], function ($, util) {
  var eventMaker = util.Module("eventMaker");

  eventMaker.performSwipeRight = function (target, element) {
    target = $(target)[0];
    var event = document.createEvent("UIEvents");
    event.initEvent('swiperight', true, true);
    target.dispatchEvent(event);
  }

  eventMaker.performSwipeLeft = function (target, element) {
    target = $(target)[0];
    var event = document.createEvent("UIEvents");
    event.initEvent('swipeleft', true, true);
    target.dispatchEvent(event);
  }

  eventMaker.performMouseEvent = function (target, eventtext) {
    target = $(target)[0];
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent(eventtext, false, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    target.dispatchEvent(event);
  }

  eventMaker.performMouseEnter = function (target) {
    target = $(target)[0];
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent("mouseenter", false, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    target.dispatchEvent(event);
  }

  eventMaker.performMouseOver = function (target) {
    target = $(target)[0];
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent("mouseover", false, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    target.dispatchEvent(event);
  }

  eventMaker.performMouseLeave = function (target) {
    target = $(target)[0];
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent("mouseleave", false, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    target.dispatchEvent(event);
  }

  eventMaker.performTap = function (target) {
    target = $(target)[0];
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent("tap", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    target.dispatchEvent(event);
  }

  eventMaker.performClick = function (target) {
    // FIXME: should accept other parameters, like Ctrl/Alt/etc
    var event = document.createEvent("MouseEvents");
    event.initMouseEvent(
      "click", // type
      true, // canBubble
      true, // cancelable
      window, // view
      0, // detail
      0, // screenX
      0, // screenY
      0, // clientX
      0, // clientY
      false, // ctrlKey
      false, // altKey
      false, // shiftKey
      false, // metaKey
      0, // button
      null // relatedTarget
    );
    // FIXME: I'm not sure this custom attribute always propagates?
    // seems okay in Firefox/Chrome, but I've had problems with
    // setting attributes on keyboard events in the past.
    event.togetherjsInternal = true;
    target = $(target)[0];
    var cancelled = target.dispatchEvent(event);
    if (cancelled) {
      return;
    }
    if (target.tagName == "A") {
      var href = target.href;
      if (href) {
        location.href = href;
        return;
      }
    }
    // FIXME: should do button clicks (like a form submit)
    // FIXME: should run .onclick() as well
  };

  eventMaker.fireChange = function (target) {
    target = $(target)[0];
    var event = document.createEvent("HTMLEvents");
    event.initEvent("change", true, true);
    target.dispatchEvent(event);
  };

  return eventMaker;
});
