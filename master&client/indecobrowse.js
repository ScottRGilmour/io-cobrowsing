function setConfig(configName, configVal) {
    if (TogetherJS && TogetherJS.config) {
        TogetherJS.config(configName, configVal);
    }
}

function setRoom() {
    let inputText = document.getElementById('roomId');
    setConfig('findRoom', inputText.value);
}

function setName() {
    let inputText = document.getElementById('nameId');
    setConfig('getUserName', function () {
        return inputText.value;
    });

}

function updateSessionStatus() {
    if (TogetherJS && TogetherJS.require) {
        session = TogetherJS.require("session");
        document.getElementById('identity').innerText = session.identityId;
        document.getElementById('room').innerText = session.shareId;
    } else {
        if (TogetherJS && TogetherJS.config) {
            document.getElementById('room').innerText = TogetherJS.config.get('findRoom');
        }
    }
}

function updateMaster() {
    let element = document.getElementById('master');
    let h1text = document.getElementById('mastertxt');
    switch (element.value) {
        case 'master':
        setConfig('master', true);
        h1text.innerText = 'Master';
        break;
        case 'slave':
        setConfig('master', false);
        h1text.innerText = 'Slave';
        
        break;
    }
}

window.onload = function () {
    intervalCheckStatus();
}

function intervalCheckStatus() {
    setInterval(function () {
        if (TogetherJS.running) {
            document.getElementById('startStopBtn').innerText = 'Stop';
        } else {
            document.getElementById('startStopBtn').innerText = 'Start';
        }

        document.getElementById('statusID').innerText = TogetherJS.running;

        updateSessionStatus();
    }, 1200);
}

function openShare() {
    window.open(TogetherJS.shareUrl());
}

function handleIFrameCoBrowsing() {
    let window = document.getElementById('frame');
    if (window.contentWindow) {
        TogetherJS(window.contentWindow);
    }
}

function handleCoBrowsing() {
    TogetherJS(this);
    return false;
}